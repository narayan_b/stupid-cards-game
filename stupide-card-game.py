from random import shuffle

cards = [0, 1, 1, 2, 2, 3, 3, 4, 4, 5]
shuffle(cards)

robot_score = 0
human_score = 0


def Robert():
    return cards.pop(0)
        
def Human():
    return cards.pop(0)
            
for round_number in range(5):
    print(f"Round {round_number + 1}")

    robot_card = Robert()
    human_card = Human()

    print(f"Robert plays : {robot_card}")
    print(f"Human plays : {human_card}")

    if robot_card > human_card:
        robot_score = robot_score + 1
        print("Robot wins this round.")
    elif robot_card == human_card:
        robot_score = robot_score + 1
        print("Robot wins this round.")
    else: human_score = human_score + 1
    print("Human wins this round.")



